<?php

######################
# This is the class required to compress your folder contents into a neat ZIP file
# Start HZip class

class HZip
{
  /**
   * Add files and sub-directories in a folder to zip file.
   * @param string $folder
   * @param ZipArchive $zipFile
   * @param int $exclusiveLength Number of text to be exclusived from the file path.
   */
  private static function folderToZip($folder, &$zipFile, $exclusiveLength) {
    $handle = opendir($folder);
    while (false !== $f = readdir($handle)) {
      if ($f != '.' && $f != '..') {
        $filePath = "$folder/$f";
        // Remove prefix from file path before add to zip.
        $localPath = substr($filePath, $exclusiveLength);
        if (is_file($filePath)) {
          $zipFile->addFile($filePath, $localPath);
        } elseif (is_dir($filePath)) {
          // Add sub-directory.
          $zipFile->addEmptyDir($localPath);
          self::folderToZip($filePath, $zipFile, $exclusiveLength);
        }
      }
    }
    closedir($handle);
  }

  /**
   * Zip a folder (include itself).
   * Usage:
   *   HZip::zipDir('/path/to/sourceDir', '/path/to/out.zip');
   *
   * @param string $sourcePath Path of directory to be zip.
   * @param string $outZipPath Path of output zip file.
   */
  public static function zipDir($sourcePath, $outZipPath)
  {
    $pathInfo = pathInfo($sourcePath);
    $parentPath = $pathInfo['dirname'];
    $dirName = $pathInfo['basename'];

    $z = new ZipArchive();
    $z->open($outZipPath, ZIPARCHIVE::CREATE);
    $z->addEmptyDir($dirName);
    self::folderToZip($sourcePath, $z, strlen("$parentPath/"));
    $z->close();
  }
}

# End HZip class

######################
# Now, we need to get the time and date stamp according to your selected timezone in order to append this to your file name
date_default_timezone_set('Asia/Manila');
$tds=date('M_j_Y_Hi');

######################
# Use HZip to compress config, content, and theme folders and create a zip file with timestamp
HZip::zipDir('path/to/config', 'path/to/config-'.$tds.'.zip');
HZip::zipDir('path/to/content', 'path/to/content-'.$tds.'.zip');
HZip::zipDir('/path/to/themes/theme-name', 'path/to/theme-'.$tds.'.zip');

######################
# Select the zip files to attach to email
$configfile='path/to/config-'.$tds.'.zip';
$contentfile='path/to/content-'.$tds.'.zip';
$themefile='path/to/theme-'.$tds.'.zip';

######################
# Email part, you need to setup an account with Mailgun for this and install Mailgun on your server
# Include the Autoloader (see "Libraries" for install instructions)
require 'mailgun-php/vendor/autoload.php';
use Mailgun\Mailgun;

# First, instantiate the SDK with your API credentials and define your domain.
$mg = new Mailgun("key-YourMailgunKeyHere");
$domain = "YourDomain.com";

# Next, instantiate a Message Builder object from the SDK.
$messageBldr = $mg->MessageBuilder();

# Define the from address.
$messageBldr->setFromAddress("mail@YourDomain.com", array("first"=>"Website", "last" => "Postmaster"));

# Define a to recipient.
$messageBldr->addToRecipient("your@email.com", array("first" => "Your", "last" => "Name"));

# Define the subject.
$messageBldr->setSubject("Backup for ".$tds);

# Define the body of the message.
$messageBldr->setTextBody("Hi! \n\nThis is the current backup for your website. \n\n\n;)");

# Other Optional Parameters.
$messageBldr->addAttachment($configfile);
$messageBldr->addAttachment($contentfile);
$messageBldr->addAttachment($themefile);


# Finally, send the message.
$mg->post("{$domain}/messages", $messageBldr->getMessage(), $messageBldr->getFiles());

?>
