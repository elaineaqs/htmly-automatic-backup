# HTMLy Automatic Backup

A simple automatic backup system for flat file CMS, HTMLy by @danpros (danpros.com).

## More information

You may find the accompanying blog post for this code [here](https://erca.space/day-14-htmly-automatic-backup-system/).